﻿using Domain.ActivityStates;
using Domain.Actors;

namespace Domain
{
    public class Activity
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public Developer? Asignee;

        public ActivityStatus Status;

        public Activity(string name, string description)
        {
            Name = name;
            Description = description;
            Status = new ToDo(this);
            Asignee = null;
        }

        public void SetAginee(Developer developer)
        {
            Asignee = developer;
        }

        public void SetItemStatus(ActivityStatus status)
        {
            Status = status;
        }
    }
}
