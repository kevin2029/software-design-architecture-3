﻿namespace Domain.Actors
{
    public class Developer : Actor
    {
        public Developer(string name, string email, string PhoneNumber) : base(name, email, PhoneNumber)
        {
        }
    }
}
