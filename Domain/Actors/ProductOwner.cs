﻿namespace Domain.Actors
{
    public class ProductOwner : Actor
    {
        public ProductOwner(string name, string email, string PhoneNumber) : base(name, email, PhoneNumber)
        {
        }
    }
}
