﻿namespace Domain.Actors
{
    public abstract class Actor
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Number { get; set; }

        protected Actor(string name, string email, string PhoneNumber)
        {
            Name = name;
            Email = email;
            Number = PhoneNumber;
        }
    }
}
