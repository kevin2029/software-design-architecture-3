﻿using Domain.Composites;
using Domain.Visitors;

namespace Domain.Thread
{
    public class Comment : Composite
    {
        public string Description { get; set; }

        public Comment(string description)
        {
            Description = description;
        }

        public void SetDescription(string description)
        {
            Description = description;
        }

        public override void AcceptVisitor(Object obj, int depth)
        {
            Visitor? visitor = obj as Visitor;
            visitor?.VisitComment(this, depth);
            base.AcceptVisitor(visitor, depth);
        }
    }
}
