﻿using Domain.Composites;
using Domain.Visitors;

namespace Domain.Thread
{
    public class ThreadItem : Composite
    {
        public override void AcceptVisitor(Object obj, int depth)
        {
            Visitor? visitor = obj as Visitor;
            visitor?.VisitThread(this);
            base.AcceptVisitor(visitor, depth);
        }
    }
}
