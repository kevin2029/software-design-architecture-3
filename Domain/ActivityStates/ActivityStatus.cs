﻿namespace Domain.ActivityStates
{
    public abstract class ActivityStatus
    {
        public Activity Activity { get; set; }

        protected ActivityStatus(Activity activity)
        {
            Activity = activity;
        }

        public abstract void Pickup();
        public abstract void GiveUp();
        public abstract void Finish();
    }
}
