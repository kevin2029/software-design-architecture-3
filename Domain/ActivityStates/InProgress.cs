﻿namespace Domain.ActivityStates
{
    public class InProgress : ActivityStatus
    {
        public InProgress(Activity activity) : base(activity)
        {
        }

        public override void Finish()
        {
            Activity.SetItemStatus(new Done(Activity));
            Console.WriteLine("Activity is finished.");
        }

        public override void GiveUp()
        {
            Activity.SetItemStatus(new ToDo(Activity));
            Console.WriteLine("Activity going back to Todo");
        }

        public override void Pickup()
        {
            Console.WriteLine("This Activity is already picked.");
        }
    }
}
