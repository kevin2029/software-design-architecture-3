﻿namespace Domain.ActivityStates
{
    public class Done : ActivityStatus
    {
        public Done(Activity activity) : base(activity)
        {
        }

        public override void Finish()
        {
            Console.WriteLine("Activity is finished.");
        }

        public override void GiveUp()
        {
            Console.WriteLine("This Activity is already done.");
        }

        public override void Pickup()
        {
            Console.WriteLine("Activity is already finished.");
        }
    }
}
