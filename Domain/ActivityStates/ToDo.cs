﻿namespace Domain.ActivityStates
{
    public class ToDo : ActivityStatus
    {
        public ToDo(Activity activity) : base(activity)
        {
        }

        public override void Finish()
        {
            Console.WriteLine("This Activity is not picked up.");
        }

        public override void GiveUp()
        {
            Console.WriteLine("Activity is not picked up.");
        }

        public override void Pickup()
        {
            Activity.SetItemStatus(new InProgress(Activity));
            Console.WriteLine("Activity is picked up.");
        }
    }
}
