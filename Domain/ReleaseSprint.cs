﻿using Domain.Publisher;
using Domain.SprintStates;

namespace Domain
{
    public class ReleaseSprint : Sprint, IPublisher
    {
        public List<ISubscriber> Subscribers { get; set; }
        public ReleaseSprint(string name, DateTime startDate, DateTime endDate) : base(name, startDate, endDate)
        {
            Subscribers = new List<ISubscriber>();
            Status = new Created(this);
        }

        public void NotifySubscribers()
        {
            foreach (ISubscriber subscriber in Subscribers)
            {
                subscriber.UpdateSprint(this);
            }
        }

        public override void SetSprintStatus(SprintStatus status)
        {
            Status = status;
            NotifySubscribers();
        }

        public void Subscribe(ISubscriber subscriber)
        {
            Subscribers.Add(subscriber);
        }

        public void UnSubscribe(ISubscriber subscriber)
        {
            Subscribers.Remove(subscriber);
        }
    }
}
