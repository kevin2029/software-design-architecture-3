﻿using Domain.Composites;
using Domain.Publisher;
using Domain.Visitors;

namespace Domain.CicdPipeline
{
    public class Pipeline : Composite, IPublisher
    {
        public string Name { get; set; }
        public List<ISubscriber> Subscribers;

        public Pipeline(string name)
        {
            Name = name;
            Subscribers = new List<ISubscriber>();
        }

        public void Start(Object obj, int depth)
        {
            Console.WriteLine("Pipeline started");
            AcceptVisitor(obj, depth);
        }
        public void Complete()
        {
            Console.WriteLine("Pipeline completed");
            NotifySubscribers();
        }
        public void Cancel()
        {
            Console.WriteLine("Pipeline Canceled");
            NotifySubscribers();
        }
        public void Fail()
        {
            Console.WriteLine("Pipeline failed");
            NotifySubscribers();
        }

        public override void AcceptVisitor(Object obj, int depth)
        {
            Visitor? visitor = obj as Visitor;
            visitor?.VisitPipeline(this);
            base.AcceptVisitor(visitor, depth);
        }
        public void NotifySubscribers()
        {
            foreach (ISubscriber subscriber in Subscribers)
            {
                subscriber.UpdatePipeline(this);
            }
        }

        public void Subscribe(ISubscriber subscriber)
        {
            Subscribers.Add(subscriber);
        }

        public void UnSubscribe(ISubscriber subscriber)
        {
            Subscribers.Remove(subscriber);
        }
    }
}
