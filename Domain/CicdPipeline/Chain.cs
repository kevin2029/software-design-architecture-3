﻿using Domain.Composites;
using Domain.Visitors;

namespace Domain.CicdPipeline
{
    public class Chain : Composite
    {
        public string Name { get; set; }

        public Chain(string name)
        {
            Name = name;
        }

        public override void AcceptVisitor(Object obj, int depth)
        {
            Visitor? visitor = obj as Visitor;
            visitor?.VisitChain(this);
            base.AcceptVisitor(visitor, depth);
        }
    }
}
