﻿using Domain.Composites;
using Domain.Visitors;

namespace Domain.CicdPipeline
{
    public class Command : Component
    {
        public string Name { get; set; }

        public Command(string command)
        {
            Name = command;
        }
        public override void AcceptVisitor(object obj, int depth)
        {
            Visitor? visitor = obj as Visitor;
            visitor?.VisitCommand(this, depth);
        }
    }
}
