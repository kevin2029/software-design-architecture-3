﻿using Domain.Actors;

namespace Domain
{
    public class Project
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public List<Sprint> Sprints { get; set; }

        public Backlog Backlog { get; set; }

        public List<Developer> Developers { get; set; }

        public ScrumMaster ScrumMaster { get; set; }

        public ProductOwner ProductOwner { get; set; }


        public Project(string name, string description, ScrumMaster scrumMaster, ProductOwner productOwner)
        {
            Name = name;
            Description = description;
            Sprints = new List<Sprint>();
            Backlog = new Backlog();
            Developers = new List<Developer>();
            ScrumMaster = scrumMaster;
            ProductOwner = productOwner;

        }
        public void AddDeveloper(Developer developer)
        {
            Developers.Add(developer);
        }

        public void AddSprint(Sprint sprint)
        {
            Sprints.Add(sprint);
        }
    }
}
