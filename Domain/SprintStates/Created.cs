﻿namespace Domain.SprintStates
{
    public class Created : SprintStatus
    {
        public Created(Sprint sprint) : base(sprint)
        {
        }

        public override void Complete()
        {
            Console.WriteLine("Sprint already completed.");
        }

        public override void Finish()
        {
            Console.WriteLine("Sprint already finished.");
        }

        public override void Release()
        {
            Console.WriteLine("Sprint already released.");
        }

        public override void Review()
        {
            Console.WriteLine("Sprint already reviewed.");
        }

        public override void Start()
        {
            Sprint.SetSprintStatus(new InProgress(Sprint));
            Console.WriteLine("Sprint is starting.");
        }
    }
}
