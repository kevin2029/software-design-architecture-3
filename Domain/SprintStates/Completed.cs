﻿namespace Domain.SprintStates
{
    public class Completed : SprintStatus
    {
        public Completed(Sprint sprint) : base(sprint)
        {
        }

        public override void Complete()
        {
            Console.WriteLine("Sprint already completed.");
        }

        public override void Finish()
        {
            Console.WriteLine("Sprint already finished.");
        }

        public override void Release()
        {
            Console.WriteLine("Sprint already released.");
        }

        public override void Review()
        {
            Console.WriteLine("Sprint already reviewed.");
        }

        public override void Start()
        {
            Console.WriteLine("Sprint already started.");
        }
    }
}
