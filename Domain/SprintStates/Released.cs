﻿namespace Domain.SprintStates
{
    public class Released : SprintStatus
    {
        public Released(Sprint sprint) : base(sprint)
        {
        }

        public override void Complete()
        {
            Sprint.SetSprintStatus(new Completed(Sprint));
            Console.WriteLine("Sprint is completed.");
        }

        public override void Finish()
        {
            Console.WriteLine("Sprint already finished.");
        }

        public override void Release()
        {
            Console.WriteLine("Sprint already released.");
        }

        public override void Review()
        {
            Console.WriteLine("Sprint already reviewed.");
        }

        public override void Start()
        {
            Console.WriteLine("Sprint already started.");
        }
    }
}
