﻿using Domain.Visitors;

namespace Domain.SprintStates
{
    public class Finished : SprintStatus
    {
        public Finished(Sprint sprint) : base(sprint)
        {
        }

        public override void Complete()
        {
            Console.WriteLine("Sprint has to be released or reviewed before completing.");
        }

        public override void Finish()
        {
            Console.WriteLine("Sprint already finished.");
        }

        public override void Release()
        {
            if (Sprint is ReleaseSprint)
            {
                Sprint.SetSprintStatus(new Released(Sprint));
                Sprint.ExecutePipeline(new ConcreteVisitor(), 0);
                Console.WriteLine("Sprint is being released.");
            }
        }

        public override void Review()
        {
            if (Sprint is ReviewSprint)
            {
                Sprint.SetSprintStatus(new Reviewed(Sprint));
                Console.WriteLine("Sprint is being reviewed.");
            }
        }

        public override void Start()
        {
            Console.WriteLine("Sprint already started.");
        }
    }
}
