﻿namespace Domain.SprintStates
{
    public abstract class SprintStatus
    {
        public Sprint Sprint { get; set; }

        protected SprintStatus(Sprint sprint)
        {
            Sprint = sprint;
        }

        public abstract void Start();
        public abstract void Finish();
        public abstract void Review();
        public abstract void Release();
        public abstract void Complete();
    }
}
