﻿namespace Domain.ItemStates
{
    public class Tested : ItemStatus
    {
        public Tested(BacklogItem backlogItem) : base(backlogItem)
        {
        }

        public override void Complete()
        {
            bool ActivityDoneAll = true;
            foreach (Activity activity in Item.Activities)
            {
                if (activity.Status.GetType() != new ActivityStates.Done(activity).GetType())
                {
                    ActivityDoneAll = false;
                }
            }

            if (ActivityDoneAll)
            {
                Item.SetItemStatus(new Done(Item));
                Item.NotifySubscribers();
                Console.WriteLine("This backlog item is completed.");
            }
            else
            {
                Console.WriteLine("Not all activities are done.");
            }

        }

        public override void DoneTesting()
        {
            Console.WriteLine("backlog item finished testing.");
        }

        public override void DoOver()
        {
            Console.WriteLine("backlog item is already has been tested and approved");
        }

        public override void Finish()
        {
            Item.SetItemStatus(new ReadyForTesting(Item));
            Console.WriteLine("backlog item back to ready for testing, test not succesful");
        }

        public override void PickItem()
        {
            Console.WriteLine("backlog item is already picked");
        }

        public override void Test()
        {
            Console.WriteLine("backlog item is already in testing");
        }
    }
}
