﻿namespace Domain.ItemStates
{
    public class Done : ItemStatus
    {
        public Done(BacklogItem backlogItem) : base(backlogItem)
        {
        }

        public override void Complete()
        {
            Console.WriteLine("backlog item has to be tested first.");
        }

        public override void DoneTesting()
        {
            Console.WriteLine("backlog item already finshed testing");
        }

        public override void DoOver()
        {
            Item.SetItemStatus(new Todo(Item));
            Item.NotifySubscribers();
            Console.WriteLine("backlog item going back to Todo.");
        }

        public override void Finish()
        {
            Console.WriteLine("backlog item is already finished");
        }

        public override void PickItem()
        {
            Console.WriteLine("backlog item is already picked");
        }

        public override void Test()
        {
            Console.WriteLine("backlog item is already tested");
        }
    }
}
