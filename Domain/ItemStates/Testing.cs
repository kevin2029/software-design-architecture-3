﻿namespace Domain.ItemStates
{
    public class Testing : ItemStatus
    {
        public Testing(BacklogItem backlogItem) : base(backlogItem)
        {
        }

        public override void Complete()
        {
            Console.WriteLine("backlog item has to be finsished testing first.");
        }

        public override void DoneTesting()
        {
            Item.SetItemStatus(new Tested(Item));
            Console.WriteLine("backlog item finished testing.");
        }

        public override void DoOver()
        {
            Item.SetItemStatus(new Todo(Item));
            Item.NotifySubscribers();
            Console.WriteLine("backlog item going back to Todo.");
        }

        public override void Finish()
        {
            Console.WriteLine("backlog item is already finished");
        }

        public override void PickItem()
        {
            Console.WriteLine("backlog item is already picked");
        }

        public override void Test()
        {
            Console.WriteLine("backlog item is already in testing");
        }
    }
}
