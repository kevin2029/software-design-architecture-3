﻿namespace Domain.ItemStates
{

    public class Todo : ItemStatus
    {

        public Todo(BacklogItem backlogItem) : base(backlogItem)
        {

        }
        public override void Complete()
        {
            Console.WriteLine("backlog item has to be tested first.");
        }

        public override void DoneTesting()
        {
            Console.WriteLine("backlog item has to be tested first.");
        }

        public override void DoOver()
        {
            Console.WriteLine("backlog item has to be tested first.");
        }

        public override void Finish()
        {
            Console.WriteLine("backlog item has to be in progress first.");
        }

        public override void PickItem()
        {
            Item.SetItemStatus(new InProgress(Item));
            Console.WriteLine("backlog item status changed to In Progress");
        }

        public override void Test()
        {
            Console.WriteLine("backlog item has to be ready for testing first.");
        }
    }
}
