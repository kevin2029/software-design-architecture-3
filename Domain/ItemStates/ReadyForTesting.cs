﻿namespace Domain.ItemStates
{
    public class ReadyForTesting : ItemStatus
    {
        public ReadyForTesting(BacklogItem backlogItem) : base(backlogItem)
        {
        }

        public override void Complete()
        {
            Console.WriteLine("backlog item has to be tested first.");
        }

        public override void DoneTesting()
        {
            Console.WriteLine("backlog item has to be tested first.");
        }

        public override void DoOver()
        {
            Console.WriteLine("backlog item has to be tested first.");
        }

        public override void Finish()
        {
            Console.WriteLine("backlog item is already finished");
        }

        public override void PickItem()
        {
            Console.WriteLine("backlog item is already picked");
        }

        public override void Test()
        {
            Item.SetItemStatus(new Testing(Item));
            Item.NotifySubscribers();
            Console.WriteLine("This Item is being tested.");
        }
    }
}
