﻿namespace Domain.ItemStates
{
    public abstract class ItemStatus
    {
        public BacklogItem Item { get; set; }

        protected ItemStatus(BacklogItem backlogItem)
        {
            Item = backlogItem;
        }
        public abstract void PickItem();
        public abstract void Finish();
        public abstract void Test();
        public abstract void DoneTesting();
        public abstract void Complete();
        public abstract void DoOver();

    }
}
