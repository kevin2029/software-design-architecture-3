﻿namespace Domain.ItemStates
{
    public class InProgress : ItemStatus
    {
        public InProgress(BacklogItem backlogItem) : base(backlogItem)
        {
        }

        public override void Complete()
        {
            Console.WriteLine("backlog item has to be tested first.");
        }

        public override void DoneTesting()
        {
            Console.WriteLine("backlog item has to be tested first.");
        }

        public override void DoOver()
        {
            Console.WriteLine("backlog item has to be tested first.");
        }

        public override void Finish()
        {
            Item.SetItemStatus(new ReadyForTesting(Item));
            Item.NotifySubscribers();
            Console.WriteLine("This backlog item is ready for testing");
        }

        public override void PickItem()
        {
            Console.WriteLine("backlog item is already picked");
        }

        public override void Test()
        {
            Console.WriteLine("backlog item has to be ready for testing first.");
        }
    }
}
