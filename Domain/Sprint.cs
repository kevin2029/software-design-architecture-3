﻿using Domain.CicdPipeline;
using Domain.ReportExport;
using Domain.SprintStates;

namespace Domain
{
    public abstract class Sprint
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        private List<BacklogItem> items;
        public SprintStatus? Status { get; set; }

        private Director Director { get; set; }

        public Pipeline? Pipeline { get; set; }

        protected Sprint(string name, DateTime startDate, DateTime endDate)
        {
            Name = name;
            StartDate = startDate;
            EndDate = endDate;
            items = new List<BacklogItem>();
            Director = new Director();
        }

        public abstract void SetSprintStatus(SprintStatus status);


        public Report GenerateReport(string title, string content, string? footer, string? header, IExportFormat exportFormat)
        {
            ConcreteReportBuilder reportBuilder = new();
            Director.SetBuilder(reportBuilder);

            return Director.ContructReport(title, content, footer, header, exportFormat);
        }

        public void ExecutePipeline(Object obj, int depth)
        {
            if (Status?.GetType() != new Finished(this).GetType())
            {
                Console.WriteLine("Can not start pipeline, Sprint is not finished.");
                return;
            }
            Pipeline?.Start(obj, depth);
        }

        public void SetName(String name)
        {
            if (StateCreatedCheck())
            {
                Name = name;
            }
            else
            {
                Console.WriteLine("sprint is nto in created state");
            }
        }

        public void SetStartDate(DateTime startDate)
        {
            if (StateCreatedCheck())
            {
                StartDate = startDate;
            }
        }
        public void SetEndDate(DateTime endDate)
        {
            if (StateCreatedCheck())
            {
                EndDate = endDate;
            }
        }

        private bool StateCreatedCheck()
        {
            return (Status?.GetType() == new Created(this).GetType());
        }
    }
}
