﻿namespace Domain.ReportExport
{
    public class ConcreteReportBuilder : IReportBuilder
    {
        public Report Report = new Report();

        public Report GetReport()
        {
            return Report;
        }

        public ConcreteReportBuilder SetContent(string content)
        {
            Report.Content = content;
            return this;
        }

        public ConcreteReportBuilder SetExport(IExportFormat exportformat)
        {
            Report.Export = exportformat;
            return this;
        }

        public ConcreteReportBuilder SetFooter(string footer)
        {
            Report.Footer = footer;
            return this;
        }

        public ConcreteReportBuilder SetHeader(string header)
        {
            Report.Header = header;
            return this;
        }

        public ConcreteReportBuilder SetTitle(string title)
        {
            Report.Title = title;
            return this;
        }
    }
}
