﻿namespace Domain.ReportExport
{
    public interface IReportBuilder
    {
        ConcreteReportBuilder SetTitle(string title);
        ConcreteReportBuilder SetHeader(string header);
        ConcreteReportBuilder SetFooter(string footer);
        ConcreteReportBuilder SetContent(string content);
        ConcreteReportBuilder SetExport(IExportFormat exportformat);
        Report GetReport();

    }
}
