﻿namespace Domain.ReportExport
{
    public class Report
    {
        public string Title { get; set; }
        public string Header { get; set; }
        public string Content { get; set; }
        public string Footer { get; set; }
        public IExportFormat Export { get; set; }

        public void ExportReport()
        {
            Export.ExportReport(this);
        }

        public void DisplayReport()
        {
            Console.WriteLine("Title :" + Title);
            Console.WriteLine("Header :" + Header);
            Console.WriteLine("Content :" + Content);
            Console.WriteLine("Footer :" + Footer);
        }
    }
}
