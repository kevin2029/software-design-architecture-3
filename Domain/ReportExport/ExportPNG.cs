﻿namespace Domain.ReportExport
{
    public class ExportPng : IExportFormat
    {
        public void ExportReport(Report report)
        {
            Console.WriteLine("Exported to PNG");
        }
    }
}
