﻿namespace Domain.ReportExport
{
    public class ExportJpg : IExportFormat
    {
        public void ExportReport(Report report)
        {
            Console.WriteLine("Exported to JPG");
        }
    }
}
