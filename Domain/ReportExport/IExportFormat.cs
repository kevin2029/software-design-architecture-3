﻿namespace Domain.ReportExport
{
    public interface IExportFormat
    {
        void ExportReport(Report report);
    }
}
