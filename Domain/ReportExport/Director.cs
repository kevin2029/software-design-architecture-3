﻿namespace Domain.ReportExport
{
    public class Director
    {
        private IReportBuilder? _ReportBuilder;


        public void SetBuilder(IReportBuilder builder)
        {
            _ReportBuilder = builder;
        }

        public Report ContructReport(string title, string content, string? footer, string? header, IExportFormat exportFormat)
        {
            //only footer is filled
            if (header == null && footer != null)
            {
                return _ReportBuilder.SetTitle(title).SetContent(content).SetFooter(footer).SetExport(exportFormat).GetReport();
            }

            //only header is filled
            if (header != null && footer == null)
            {
                return _ReportBuilder.SetTitle(title).SetContent(content).SetHeader(header).SetExport(exportFormat).GetReport();
            }

            //both footer and header are filled
            if (header != null && footer != null)
            {
                return _ReportBuilder.SetTitle(title).SetContent(content).SetHeader(header).SetFooter(footer).SetExport(exportFormat).GetReport();
            }

            // both footer and header are null
            return _ReportBuilder.SetTitle(title).SetContent(content).SetExport(exportFormat).GetReport();

        }

    }
}
