﻿using Domain.Actors;
using Domain.ItemStates;
using Domain.Publisher;
using Domain.Thread;

namespace Domain
{
    public class BacklogItem : IPublisher
    {
        public int Id { get; set; }

        public string Description { get; set; }
        public Developer? Asignee { get; set; }

        public ItemStatus Status { get; set; }

        private List<ISubscriber> Subscribers { get; set; }

        public List<Activity> Activities { get; set; }

        public ThreadItem Thread;

        public BacklogItem(int id, string description)
        {
            Id = id;
            Description = description;
            Status = new Todo(this);
            Subscribers = new List<ISubscriber>();
            Activities = new List<Activity>();
            Asignee = null;
            Thread = new ThreadItem();

        }
        public void SetItemStatus(ItemStatus status)
        {
            Status = status;
        }

        public void AddComment(Comment comment, Comment? MainComment)
        {
            if (Status.GetType() != typeof(Done))
            {
                if (MainComment != null)
                {
                    MainComment.AddComponent(comment);
                    NotifySubscribers();
                }
                else
                {
                    Thread.AddComponent(comment);
                }
                NotifySubscribers();
            }
            else
            {
                Console.WriteLine("backlog item is done, no changes allowed!");
            }
        }

        public void AlterComment(Comment comment, string NewDescription)
        {
            if (Status.GetType() != typeof(Done))
            {
                comment.SetDescription(NewDescription);
                NotifySubscribers();
            }
            else
            {
                Console.WriteLine("backlog item is done, no changes allowed!");
            }

        }

        public void SetAsignee(Developer developer)
        {
            Asignee = developer;

        }
        public void AddActivity(Activity activity)
        {
            Activities.Add(activity);
        }

        public void NotifySubscribers()
        {
            foreach (ISubscriber subscriber in Subscribers)
            {
                subscriber.Update(this);
            }
        }

        public void Subscribe(ISubscriber subscriber)
        {
            Subscribers.Add(subscriber);
        }

        public void UnSubscribe(ISubscriber subscriber)
        {
            Subscribers.Remove(subscriber);
        }
    }
}
