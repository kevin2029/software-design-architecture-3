﻿namespace Domain.Composites
{
    public class Composite : Component
    {
        private readonly List<Component> components;

        public Composite()
        {
            components = new List<Component>();
        }

        public void AddComponent(Component component)
        {
            components.Add(component);
        }

        public override void AcceptVisitor(object visitor, int depth)
        {
            foreach (Component component in components)
            {
                component.AcceptVisitor(visitor, depth + 2);
            }
        }
    }
}
