﻿namespace Domain.Composites
{
    public abstract class Component
    {
        public abstract void AcceptVisitor(Object visitor, int depth);
    }
}
