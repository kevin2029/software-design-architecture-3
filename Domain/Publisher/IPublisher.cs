﻿namespace Domain.Publisher
{
    public interface IPublisher
    {
        void Subscribe(ISubscriber subscriber);
        void UnSubscribe(ISubscriber subscriber);
        void NotifySubscribers();
    }
}
