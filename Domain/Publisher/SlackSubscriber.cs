﻿using Domain.CicdPipeline;

namespace Domain.Publisher
{
    public class SlackSubscriber : ISubscriber
    {
        public string Username { get; set; }

        public SlackSubscriber(string username)
        {
            Username = username;
        }
        public void Update(BacklogItem context)
        {
            Console.WriteLine("Slack send for the backlogitem: " + context.Id);
        }

        public void UpdateSprint(Sprint sprint)
        {
            Console.WriteLine("Slack send for the sprint: " + sprint.Name);
        }

        public void UpdatePipeline(Pipeline pipeline)
        {
            Console.WriteLine("Slack send for the pipeline: " + pipeline.Name);
        }
    }
}
