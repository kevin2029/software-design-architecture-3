﻿using Domain.CicdPipeline;

namespace Domain.Publisher
{
    public class SmsSubscriber : ISubscriber
    {
        public string PhoneNumber { get; set; }

        public SmsSubscriber(string number)
        {
            PhoneNumber = number;
        }
        public void Update(BacklogItem context)
        {
            Console.WriteLine("Sms send for the backlogitem: " + context.Id);
        }

        public void UpdateSprint(Sprint sprint)
        {
            Console.WriteLine("Sms send for the sprint: " + sprint.Name);
        }

        public void UpdatePipeline(Pipeline pipeline)
        {
            Console.WriteLine("Sms send for the pipeline: " + pipeline.Name);
        }
    }
}
