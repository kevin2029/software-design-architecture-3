﻿using Domain.CicdPipeline;

namespace Domain.Publisher
{
    public interface ISubscriber
    {
        void Update(BacklogItem context);
        void UpdateSprint(Sprint sprint);
        void UpdatePipeline(Pipeline pipeline);
    }
}
