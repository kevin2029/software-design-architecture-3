﻿using Domain.CicdPipeline;

namespace Domain.Publisher
{
    public class MailSubscriber : ISubscriber
    {
        public string MailAddress;

        public MailSubscriber(string mailAddress)
        {
            MailAddress = mailAddress;
        }

        public void Update(BacklogItem context)
        {
            Console.WriteLine("Mail send for the backlogitem: " + context.Id);
        }

        public void UpdatePipeline(Pipeline pipeline)
        {
            Console.WriteLine("Mail send for the pipeline: " + pipeline.Name);
        }

        public void UpdateSprint(Sprint sprint)
        {
            Console.WriteLine("Mail send for the sprint: " + sprint.Name);
        }
    }
}
