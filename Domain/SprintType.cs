﻿namespace Domain
{
    public enum SprintType
    {
        Release,
        Review,
        Other
    }
}
