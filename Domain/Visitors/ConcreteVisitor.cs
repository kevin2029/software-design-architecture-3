﻿using Domain.CicdPipeline;
using Domain.Thread;

namespace Domain.Visitors
{
    public class ConcreteVisitor : Visitor
    {
        public override void VisitChain(Chain chain)
        {
            Console.WriteLine("-" + chain.Name);
        }

        public override void VisitCommand(Command command, int depth)
        {
            Console.WriteLine(new String('-', depth) + command.Name);
        }

        public override void VisitComment(Comment comment, int depth)
        {
            Console.WriteLine(new String('-', depth) + comment.Description);
        }

        public override void VisitPipeline(Pipeline pipeline)
        {
            Console.WriteLine("---" + pipeline.Name + "---");
        }

        public override void VisitThread(ThreadItem thread)
        {
            Console.WriteLine("Thread:");
        }
    }
}
