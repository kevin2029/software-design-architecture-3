﻿using Domain.CicdPipeline;
using Domain.Thread;

namespace Domain.Visitors
{
    public abstract class Visitor
    {
        public abstract void VisitThread(ThreadItem thread);
        public abstract void VisitComment(Comment comment, int depth);

        public abstract void VisitPipeline(Pipeline pipeline);

        public abstract void VisitChain(Chain chain);

        public abstract void VisitCommand(Command command, int depth);
    }
}
