﻿namespace Domain
{
    public class Backlog
    {
        public List<BacklogItem> Items { get; set; }

        public Backlog()
        {
            Items = new List<BacklogItem>();
        }

        public void AddItem(BacklogItem item)
        {
            Items.Add(item);
        }
    }
}
