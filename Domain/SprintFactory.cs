﻿namespace Domain
{
    public class SprintFactory
    {
        public Sprint CreateSprint(SprintType type, string name, DateTime startDate, DateTime endDate)
        {
            switch (type)
            {
                case SprintType.Release:
                    return new ReleaseSprint(name, startDate, endDate);
                case SprintType.Review:
                    return new ReviewSprint(name, startDate, endDate);
                case SprintType.Other:
                    throw new ArgumentNullException("Invalid SprintType type");
                default:
                    throw new ArgumentNullException("Invalid SprintType type");
            }
        }
    }
}
