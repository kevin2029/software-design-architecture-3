﻿using Domain.CicdPipeline;

namespace Tests
{
    public class PipelineTests
    {
        [Fact]
        public void ChainNameCorrectSet()
        {
            //Arrange
            Chain chain = new("testName");

            //Act
            chain.Name = "foo";

            Assert.True(chain.Name.Equals("foo"));
        }

        [Fact]
        public void CommandNameCorrectSet()
        {
            //Arrange
            Command command = new("testName");

            //Act
            command.Name = "foo";

            Assert.True(command.Name.Equals("foo"));
        }

        [Fact]
        public void PipelineNameCorrectSet()
        {
            //Arrange
            Pipeline pipeline = new("testName");

            //Act
            pipeline.Name = "foo";

            Assert.True(pipeline.Name.Equals("foo"));
        }
    }
}
