﻿using Domain;
using Domain.ReportExport;

namespace Tests
{
    public class ReportTests
    {
        [Fact]
        public void SprintReportCreatedWithHeaderAndFooter()
        {
            //arrange
            SprintFactory sprintFactory = new();
            var sprint = sprintFactory.CreateSprint(SprintType.Release, "TestName", new DateTime(), new DateTime());
            IExportFormat export = new ExportJpg();
            //act
            var Report = sprint.GenerateReport("testTitle", "testContent", "testFooter", "testHeader", export);

            //assert
            Assert.True(Report.Title.Equals("testTitle"));
            Assert.True(Report.Header.Equals("testHeader"));
            Assert.True(Report.Content.Equals("testContent"));
            Assert.True(Report.Footer.Equals("testFooter"));
        }
        [Fact]
        public void SprintReportCreatedWithOnlyHeader()
        {
            //arrange
            SprintFactory sprintFactory = new();
            var sprint = sprintFactory.CreateSprint(SprintType.Release, "TestName", new DateTime(), new DateTime());
            IExportFormat export = new ExportJpg();
            //act
            var Report = sprint.GenerateReport("testTitle", "testContent", null, "testHeader", export);

            //assert
            Assert.True(Report.Title.Equals("testTitle"));
            Assert.True(Report.Header.Equals("testHeader"));
            Assert.True(Report.Content.Equals("testContent"));
            Assert.Null(Report.Footer);
        }
        [Fact]
        public void SprintReportCreatedWithOnlyFooter()
        {
            //arrange
            SprintFactory sprintFactory = new();
            var sprint = sprintFactory.CreateSprint(SprintType.Release, "TestName", new DateTime(), new DateTime());
            IExportFormat export = new ExportJpg();
            //act
            var Report = sprint.GenerateReport("testTitle", "testContent", "testFooter", null, export);

            //assert
            Assert.True(Report.Title.Equals("testTitle"));
            Assert.True(Report.Footer.Equals("testFooter"));
            Assert.True(Report.Content.Equals("testContent"));
            Assert.Null(Report.Header);
        }

        [Fact]
        public void SprintReportCreatedWithNoHeaderFooter()
        {
            //arrange
            SprintFactory sprintFactory = new();
            var sprint = sprintFactory.CreateSprint(SprintType.Release, "TestName", new DateTime(), new DateTime());
            IExportFormat export = new ExportJpg();
            //act
            var Report = sprint.GenerateReport("testTitle", "testContent", null, null, export);

            //assert
            Assert.True(Report.Title.Equals("testTitle"));
            Assert.Null(Report.Header);
            Assert.True(Report.Content.Equals("testContent"));
            Assert.Null(Report.Footer);
        }

        [Fact]
        public void ConcreteReportBuilderSetsContent()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new ConcreteReportBuilder();

            //act
            concreteReportBuilder.SetContent("testContent");

            //assert
            Assert.True(concreteReportBuilder.Report.Content.Equals("testContent"));
        }
        [Fact]
        public void ConcreteReportBuilderSetsExport()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new ConcreteReportBuilder();
            IExportFormat export = new ExportJpg();

            //act
            concreteReportBuilder.SetExport(export);

            //assert
            Assert.True(concreteReportBuilder.Report.Export.Equals(export));
        }
        [Fact]
        public void ConcreteReportBuilderSetsFooter()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new ConcreteReportBuilder();

            //act
            concreteReportBuilder.SetFooter("test");

            //assert
            Assert.True(concreteReportBuilder.Report.Footer.Equals("test"));
        }
        [Fact]
        public void ConcreteReportBuilderSetsHeader()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new ConcreteReportBuilder();

            //act
            concreteReportBuilder.SetHeader("test");

            //assert
            Assert.True(concreteReportBuilder.Report.Header.Equals("test"));
        }

        [Fact]
        public void ConcreteReportBuilderSetsTitle()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new ConcreteReportBuilder();

            //act
            concreteReportBuilder.SetTitle("test");

            //assert
            Assert.True(concreteReportBuilder.Report.Title.Equals("test"));
        }

        [Fact]
        public void ConcreteReportBuilderReturnsReport()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new ConcreteReportBuilder();
            //act
            concreteReportBuilder.SetFooter("test");
            Report report = concreteReportBuilder.GetReport();

            //assert

            Assert.True(report.Footer.Equals("test"));
        }

        [Fact]
        public void DirectorGenerateReportWithHeaderFooter()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new();
            Director director = new();
            IExportFormat export = new ExportJpg();
            director.SetBuilder(concreteReportBuilder);

            //act
            Report report = director.ContructReport("testTitle", "testContent", "testFooter", "testHeader", export);

            //assert
            Assert.True(report.Title.Equals("testTitle"));
            Assert.True(report.Header.Equals("testHeader"));
            Assert.True(report.Content.Equals("testContent"));
            Assert.True(report.Footer.Equals("testFooter"));
        }

        [Fact]
        public void DirectorGenerateReportWithOnlyHeader()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new();
            Director director = new();
            IExportFormat export = new ExportJpg();
            director.SetBuilder(concreteReportBuilder);

            //act
            Report report = director.ContructReport("testTitle", "testContent", null, "testHeader", export);

            //assert
            Assert.True(report.Title.Equals("testTitle"));
            Assert.True(report.Header.Equals("testHeader"));
            Assert.True(report.Content.Equals("testContent"));
            Assert.Null(report.Footer);

        }

        [Fact]
        public void DirectorGenerateReportWithOnlyFooter()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new();
            Director director = new();
            IExportFormat export = new ExportJpg();
            director.SetBuilder(concreteReportBuilder);

            //act
            Report report = director.ContructReport("testTitle", "testContent", "testFooter", null, export);

            //assert
            Assert.True(report.Title.Equals("testTitle"));
            Assert.True(report.Footer.Equals("testFooter"));
            Assert.True(report.Content.Equals("testContent"));
            Assert.Null(report.Header);

        }

        [Fact]
        public void DirectorGenerateReportNoHeaderFooter()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new();
            Director director = new();
            IExportFormat export = new ExportJpg();
            director.SetBuilder(concreteReportBuilder);

            //act
            Report report = director.ContructReport("testTitle", "testContent", null, null, export);

            //assert
            Assert.True(report.Title.Equals("testTitle"));
            Assert.True(report.Content.Equals("testContent"));
            Assert.Null(report.Header);
            Assert.Null(report.Footer);

        }

        [Fact]
        public void DirectorReportExportToJpg()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new();
            Director director = new();
            IExportFormat export = new ExportJpg();
            director.SetBuilder(concreteReportBuilder);
            var writer = new StringWriter();
            Console.SetOut(writer);

            //act
            Report report = director.ContructReport("testTitle", "testContent", "testFooter", "testHeader", export);

            report.ExportReport();

            //assert
            var res = writer.ToString().Trim();
            Assert.Matches("Exported to JPG", res);

        }
        [Fact]
        public void DirectorReportExportToPng()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new();
            Director director = new();
            IExportFormat export = new ExportPdf();
            director.SetBuilder(concreteReportBuilder);

            var writer = new StringWriter();
            Console.SetOut(writer);

            //act
            Report report = director.ContructReport("testTitle", "testContent", "testFooter", "testHeader", export);
            report.ExportReport();

            //assert
            var result = writer.ToString().Trim();
            Assert.Matches("Exported to PDF", result);

        }
        [Fact]
        public void DirectorReportExportToPdf()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new();
            Director director = new();
            IExportFormat export = new ExportPdf();
            director.SetBuilder(concreteReportBuilder);
            var writer = new StringWriter();
            Console.SetOut(writer);
            //act
            Report report = director.ContructReport("testTitle", "testContent", "testFooter", "testHeader", export);
            report.ExportReport();

            //assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Exported to PDF", res);
        }
        [Fact]
        public void DirectorReportCanDisplayReport()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new();
            Director director = new();
            IExportFormat export = new ExportPdf();
            director.SetBuilder(concreteReportBuilder);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //act
            Report report = director.ContructReport("testTitle", "testContent", "testFooter", "testHeader", export);
            report.DisplayReport();
            var result = writer.ToString().Trim();

            //assert
            Assert.Matches("Title :testTitle", result);
        }

        [Fact]
        public void IExportFormatPdfCanDisplayMessage()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new();
            Director director = new();
            IExportFormat export = new ExportPdf();
            director.SetBuilder(concreteReportBuilder);
            var writer = new StringWriter();
            Console.SetOut(writer);
            //act
            Report report = director.ContructReport("testTitle", "testContent", "testFooter", "testHeader", export);
            export.ExportReport(report);

            //assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Exported to PDF", res);

        }
        [Fact]
        public void IExportFormatPngCanDisplayMessage()
        {
            //arrange
            ConcreteReportBuilder concreteReportBuilder = new();
            Director director = new();
            IExportFormat export = new ExportPng();
            director.SetBuilder(concreteReportBuilder);
            var writer = new StringWriter();
            Console.SetOut(writer);
            //act
            Report report = director.ContructReport("testTitle", "testContent", "testFooter", "testHeader", export);
            export.ExportReport(report);

            //assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Exported to PNG", res);

        }
        [Fact]
        public void IExportFormatJpgCanDisplayMessage()
        {
            //arrange

            ExportJpg export = new();

            var writer = new StringWriter();
            Console.SetOut(writer);
            //act
            Report report = new();
            report.Export = export;
            var sb = writer.GetStringBuilder();
            export.ExportReport(report);
            var res = sb.ToString().Trim();
            //assert
            Assert.Matches("Exported to JPG", res);
        }
    }
}
