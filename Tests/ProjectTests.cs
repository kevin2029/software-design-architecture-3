using Domain;
using Domain.Actors;
using Moq;

namespace Tests
{
    public class ProjectTests
    {
        [Fact]
        public void SuccessProjectCreatedWithABacklog()
        {
            //Arrange
            Mock<ProductOwner> productOwner = new("KevinPO", "PO@Test.com", "069898898");
            Mock<ScrumMaster> scrumMaster = new("Kevin", "kevin@test.com", "0666669898");
            Project project = new("TestProject", "testingNewBacklog", scrumMaster.Object, productOwner.Object);

            //Assert
            Assert.NotNull(project.Backlog);
            Assert.True(project.Sprints.Count.Equals(0));
            Assert.True(project.ScrumMaster.Equals(scrumMaster.Object));
            Assert.True(project.ProductOwner.Equals(productOwner.Object));
        }
        [Fact]
        public void SuccessActorsAddedToProject()
        {
            //Arrange
            ScrumMaster scrumMaster = new("test", "Test@test.com", "066699985");
            ProductOwner productOwner = new("test", "Test@test.com", "066699985");
            Developer developer = new("test", "Test@test.com", "066699985");

            //Act
            Project project = new("test", "testingAddingActors", scrumMaster, productOwner);
            project.AddDeveloper(developer);
            //Assert

            Assert.True(project.ScrumMaster.Equals(scrumMaster));
            Assert.True(project.Developers[0] == developer);
            Assert.True(project.ProductOwner.Equals(productOwner));
            Assert.NotNull(project.Developers);
        }

        [Fact]
        public void SuccessSprintsAddedToProject()
        {
            //Arrange
            ScrumMaster scrumMaster = new("test", "Test@test.com", "066699985");
            ProductOwner productOwner = new("test", "Test@test.com", "066699985");
            SprintFactory sprintFactory = new();
            var sprint = sprintFactory.CreateSprint(SprintType.Release, "TestName", new DateTime(), new DateTime());

            //Act
            Project project = new("test", "testingAddingActors", scrumMaster, productOwner);
            project.AddSprint(sprint);

            //Assert
            Assert.True(project.Sprints.Count.Equals(1));
            Assert.True(project.ScrumMaster.Equals(scrumMaster));
            Assert.True(project.ProductOwner.Equals(productOwner));
            Assert.True(project.Developers.Count.Equals(0));
            Assert.True(project.Sprints[0].Name.Equals("TestName"));
        }

        [Fact]
        public void SuccessProjectNameCanBeChanged()
        {
            //Arrange
            ScrumMaster scrumMaster = new("test", "Test@test.com", "066699985");
            ProductOwner productOwner = new("test", "Test@test.com", "066699985");
            Project project = new("test", "testingAddingActors", scrumMaster, productOwner);


            //Act
            project.Name = "test2";

            //Assert
            Assert.True(project.ScrumMaster.Equals(scrumMaster));
            Assert.True(project.ProductOwner.Equals(productOwner));
            Assert.True(project.Developers.Count.Equals(0));
            Assert.True(project.Name.Equals("test2"));
        }
        [Fact]
        public void SuccessProjectDescriptionCanBeChanged()
        {
            //Arrange
            ScrumMaster scrumMaster = new("test", "Test@test.com", "066699985");
            ProductOwner productOwner = new("test", "Test@test.com", "066699985");
            Project project = new("test", "testingAddingActors", scrumMaster, productOwner);


            //Act
            project.Description = "test2";

            //Assert
            Assert.True(project.ScrumMaster.Equals(scrumMaster));
            Assert.True(project.ProductOwner.Equals(productOwner));
            Assert.True(project.Developers.Count.Equals(0));
            Assert.True(project.Description.Equals("test2"));
        }

    }
}