﻿using Domain;
using Domain.Actors;
using Domain.ItemStates;

namespace Tests
{
    public class BacklogItemTests
    {
        [Fact]
        public void BacklogItemStateCreatedWithDefaultStatusTodo()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Todo toDo = new(backlogItem);

            //Act

            //Assert
            Assert.IsType(toDo.GetType(), backlogItem.Status);
        }
        [Fact]
        public void BacklogItemStateChangedToDone()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Domain.ItemStates.Done state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void BacklogItemStateChangedToInProgress()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Domain.ItemStates.InProgress state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void BacklogItemStateChangedToReadyForTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            ReadyForTesting state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void BacklogItemStateChangedToTested()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Tested state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }

        [Fact]
        public void BacklogItemStateChangedToTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Testing state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void BacklogItemStateChangedToTodo()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Todo state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TodoStateDoesNotChangeWhenCompleted()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Todo state = new(backlogItem);

            //Act
            backlogItem.Status.Complete();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TodoStateDoesNotChangeWhenDoneTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Todo state = new(backlogItem);

            //Act
            backlogItem.Status.DoneTesting();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TodoStateDoesNotChangeWhenDoingOver()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Todo state = new(backlogItem);

            //Act
            backlogItem.Status.DoOver();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TodoStateDoesNotChangeWhenFinished()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Todo state = new(backlogItem);

            //Act
            backlogItem.Status.Finish();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TodoStateDoesNotChangeWhenTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Todo state = new(backlogItem);

            //Act
            backlogItem.Status.Test();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TodoStateDoesChangeWhenPickedUp()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            InProgress state = new(backlogItem);

            //Act
            backlogItem.Status.PickItem();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void InProgressStateDoesNotChangeWhenCompleted()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            InProgress state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Complete();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void InProgressStateDoesNotChangeWhenDoneTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            InProgress state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.DoneTesting();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void InProgressStateDoesNotChangeWhenDoingOver()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            InProgress state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.DoOver();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void InProgressStateDoesNotChangeWhenPickedUp()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            InProgress state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.PickItem();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void InProgressStateDoesNotChangeWhenTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            InProgress state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Test();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void InProgressStateDoesChangeWhenFinished()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            InProgress state = new(backlogItem);
            ReadyForTesting finished = new(backlogItem);
            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Finish();

            //Assert
            Assert.IsType(finished.GetType(), backlogItem.Status);
        }
        [Fact]
        public void ReadyForTestingStateDoesNotChangeWhenCompleted()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            ReadyForTesting state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Complete();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void ReadyForTestingStateDoesNotChangeWhenDoneTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            ReadyForTesting state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.DoneTesting();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void ReadyForTestingStateDoesNotChangeWhenDoOver()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            ReadyForTesting state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.DoOver();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void ReadyForTestingStateDoesNotChangeWhenFinished()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            ReadyForTesting state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Finish();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void ReadyForTestingStateDoesNotChangeWhenPickedUp()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            ReadyForTesting state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.PickItem();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void ReadyForTestingStateDoesChangeWhenTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            ReadyForTesting state = new(backlogItem);
            Testing testing = new(backlogItem);
            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Test();

            //Assert
            Assert.IsType(testing.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestingStateDoesNotChangeWhenCompleted()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Testing state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Complete();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestingStateDoesChangeWhenDoneTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Testing state = new(backlogItem);
            Tested tested = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.DoneTesting();

            //Assert
            Assert.IsType(tested.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestingStateDoesChangeWhenDoingOver()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Testing state = new(backlogItem);
            Todo todo = new(backlogItem);
            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.DoOver();

            //Assert
            Assert.IsType(todo.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestingStateDoesNotChangeWhenFinished()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Testing state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Finish();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestingStateDoesNotChangeWhenTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Testing state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Test();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestingStateDoesNotChangeWhenPickedUp()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Testing state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.PickItem();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void DoneStateDoesNotChangeWhenCompleted()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Done state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Complete();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void DoneStateDoesNotChangeWhenDoneTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Done state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.DoneTesting();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void DoneStateDoesNotChangeWhenFinished()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Done state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Finish();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void DoneStateDoesNotChangeWhenPickedUp()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Done state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.PickItem();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void DoneStateDoesNotChangeWhenTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Done state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Test();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void DoneStateDoesChangeWhenDoingOver()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Done state = new(backlogItem);
            Todo todo = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.DoOver();

            //Assert
            Assert.IsType(todo.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestedStateDoesNotChangeWhenDoneTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Tested state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.DoneTesting();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestedStateDoesNotChangeWhenDoingOver()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Tested state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.DoOver();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestedStateDoesNotChangeWhenPickedUp()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Tested state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.PickItem();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestedStateDoesNotChangeWhenTesting()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Tested state = new(backlogItem);

            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Test();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestedStateDoesChangeWhenFinished()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Tested state = new(backlogItem);
            ReadyForTesting readyForTesting = new(backlogItem);
            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Finish();

            //Assert
            Assert.IsType(readyForTesting.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestedStateDoesChangeWhenCompletedWithoutActivities()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Tested state = new(backlogItem);
            Done done = new(backlogItem);
            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.Status.Complete();

            //Assert
            Assert.IsType(done.GetType(), backlogItem.Status);
        }
        [Fact]
        public void TestedStateDoesChangeWhenCompletedWithAllActivitiesDone()
        {

            BacklogItem backlogItem = new(1, "testItem");
            Domain.Activity activity = new("TestName", "TestDescription");
            Tested state = new(backlogItem);
            Done done = new(backlogItem);
            Domain.ActivityStates.Done done1 = new(activity);
            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.AddActivity(activity);
            activity.SetItemStatus(done1);
            backlogItem.Status.Complete();

            //Assert
            Assert.IsType(done.GetType(), backlogItem.Status);
            Assert.IsType(done1.GetType(), activity.Status);
        }

        [Fact]
        public void TestedStateDoesNotChangeWhenCompletedWithAllActivitiesNotDone()
        {

            BacklogItem backlogItem = new(1, "testItem");
            Domain.Activity activity = new("TestName", "TestDescription");
            Domain.ActivityStates.ToDo toDo = new(activity);
            Tested state = new(backlogItem);
            Done done = new(backlogItem);
            //Act
            backlogItem.SetItemStatus(state);
            backlogItem.AddActivity(activity);

            backlogItem.Status.Complete();

            //Assert
            Assert.IsType(state.GetType(), backlogItem.Status);
            Assert.IsType(toDo.GetType(), activity.Status);
        }
        [Fact]
        public void BacklogItemCreatedWithNoDeveloper()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");

            //Act

            //Assert
            Assert.Null(backlogItem.Asignee);

        }
        [Fact]
        public void BacklogItemCanSetDeveloper()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testItem");
            Developer developer = new("test", "Test@test.com", "066699985");
            //Act

            backlogItem.SetAsignee(developer);
            //Assert
            Assert.True(backlogItem.Asignee?.Equals(developer));

        }
    }
}
