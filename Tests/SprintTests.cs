﻿using Domain;
using Domain.SprintStates;

namespace Tests
{
    public class SprintTests
    {
        [Fact]
        public void SprintFactoryReturnsReleaseSprint()
        {
            //Arrange
            SprintFactory sprintFactory = new();
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            //Act
            var sprint = sprintFactory.CreateSprint(SprintType.Release, "testName", new DateTime(), new DateTime());

            //Assert
            Assert.True(sprint.GetType().Equals(releaseSprint.GetType()));
        }
        [Fact]
        public void SprintFactoryReturnsReviewSprint()
        {
            //Arrange
            SprintFactory sprintFactory = new();
            ReviewSprint reviewSprint = new("testName", new DateTime(), new DateTime());
            //Act
            var sprint = sprintFactory.CreateSprint(SprintType.Review, "testName", new DateTime(), new DateTime());

            //Assert
            Assert.True(sprint.GetType().Equals(reviewSprint.GetType()));
        }
        [Fact]
        public void SprintFactoryReturnsNullPointerExeption()
        {
            //Arrange
            SprintFactory sprintFactory = new();

            //Act
            try
            {
                var sprint = sprintFactory.CreateSprint(SprintType.Other, "testName", new DateTime(), new DateTime());

            }
            catch (ArgumentNullException e)
            {
                //Assert           
                Assert.True(e.ParamName?.Equals("Invalid sprint type."));
            }

        }

        [Fact]
        public void SprintAlteredNameWhenStateCreated()
        {
            //Arrange
            SprintFactory sprintFactory = new();
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            var sprint = sprintFactory.CreateSprint(SprintType.Release, "testName", new DateTime(), new DateTime());

            //Act
            sprint.SetName("NewName");
            //Assert
            Assert.True(sprint.GetType().Equals(releaseSprint.GetType()));
            Assert.True(sprint.Name.Equals("NewName"));
        }

        [Fact]
        public void SprintNotAlteredNameWhenStateCompleted()
        {
            //Arrange
            SprintFactory sprintFactory = new();            
            var sprint = sprintFactory.CreateSprint(SprintType.Review, "testName", new DateTime(), new DateTime());
            Completed completed = new(sprint);

            //Act
            sprint.SetSprintStatus(completed);
            sprint.SetName("NewName");
            //Assert

            Assert.True(sprint.Name.Equals("testName"));
        }

        [Fact]
        public void SprintAlteredStartDateWhenStateCreated()
        {
            //Arrange
            SprintFactory sprintFactory = new();
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            var sprint = sprintFactory.CreateSprint(SprintType.Release, "testName", new DateTime(), new DateTime());

            //Act
            sprint.SetStartDate(new DateTime(2000, 10, 06));
            //Assert
            Assert.True(sprint.GetType().Equals(releaseSprint.GetType()));
            Assert.True(sprint.StartDate.Equals(new DateTime(2000, 10, 06)));
        }

        [Fact]
        public void SprintAlteredEndDateWhenStateCreated()
        {
            //Arrange
            SprintFactory sprintFactory = new();
            var sprint = sprintFactory.CreateSprint(SprintType.Release, "testName", new DateTime(), new DateTime());

            //Act
            sprint.SetEndDate(new DateTime(2000, 10, 06));

            //Assert
            Assert.True(sprint.EndDate.Equals(new DateTime(2000, 10, 06)));
        }

        [Fact]
        public void SprintStateChangedToFinished()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            Finished state = new(releaseSprint);


            //Act
            releaseSprint.SetSprintStatus(state);

            //Assert
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateChangedToCompleted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            Completed state = new(releaseSprint);


            //Act
            releaseSprint.SetSprintStatus(state);

            //Assert
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateChangedToInProgress()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            InProgress state = new(releaseSprint);


            //Act
            releaseSprint.SetSprintStatus(state);

            //Assert
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateChangedToReleased()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            Released state = new(releaseSprint);


            //Act
            releaseSprint.SetSprintStatus(state);

            //Assert
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }

        [Fact]
        public void SprintStateCreatedDoesNotChangeWhenFinished()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            Created state = new(releaseSprint);


            //Act
            releaseSprint.Status?.Finish();

            //Assert          
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateCreatedDoesNotChangeWhenCompleted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            Created state = new(releaseSprint);


            //Act
            releaseSprint.Status?.Complete();

            //Assert          
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateCreatedDoesNotChangeWhenReleased()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            Created state = new(releaseSprint);


            //Act
            releaseSprint.Status?.Release();

            //Assert          
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateCreatedDoesNotChangeWhenreviewed()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            Created state = new(releaseSprint);


            //Act
            releaseSprint.Status?.Review();

            //Assert          
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateCreatedDoesChangeWhenStarted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            InProgress state = new(releaseSprint);


            //Act
            releaseSprint.Status?.Start();

            //Assert          
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateInProgressDoesNotChangeWhenCompleted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            InProgress state = new(releaseSprint);


            //Act
            releaseSprint.SetSprintStatus(state);
            releaseSprint.Status?.Complete();

            //Assert          
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateInProgressDoesNotChangeWhenReleased()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            InProgress state = new(releaseSprint);


            //Act
            releaseSprint.SetSprintStatus(state);
            releaseSprint.Status?.Release();

            //Assert          
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateInProgressDoesNotChangeWhenReviewed()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            InProgress state = new(releaseSprint);


            //Act
            releaseSprint.SetSprintStatus(state);
            releaseSprint.Status?.Review();

            //Assert          
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateInProgressDoesNotChangeWhenStart()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            InProgress state = new(releaseSprint);


            //Act
            releaseSprint.SetSprintStatus(state);
            releaseSprint.Status?.Start();

            //Assert          
            Assert.IsType(state.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void SprintStateInProgressDoesChangeWhenFnished()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            InProgress state = new(releaseSprint);
            Finished finshed = new(releaseSprint);

            //Act
            releaseSprint.SetSprintStatus(state);
            releaseSprint.Status?.Finish();

            //Assert          
            Assert.IsType(finshed.GetType(), releaseSprint.Status);
        }
        [Fact]
        public void ReviewSprintStateFinishedDoesNotChangeWhenReleased()
        {
            //Arrange
            ReviewSprint Sprint = new("testName", new DateTime(), new DateTime());
            Finished finished = new(Sprint);

            //Act
            Sprint.SetSprintStatus(finished);
            Sprint.Status?.Release();

            //Assert          
            Assert.IsType(finished.GetType(), Sprint.Status);
        }
        [Fact]
        public void ReleaseSprintStateFinishedDoesNotChangeWhenReviewed()
        {
            //Arrange
            ReleaseSprint Sprint = new("testName", new DateTime(), new DateTime());
            Finished finished = new(Sprint);

            //Act
            Sprint.SetSprintStatus(finished);
            Sprint.Status?.Review();

            //Assert          
            Assert.IsType(finished.GetType(), Sprint.Status);
        }
        [Fact]
        public void ReleaseSprintStateFinishedDoesChangeWhenReleased()
        {
            //Arrange
            ReleaseSprint Sprint = new("testName", new DateTime(), new DateTime());
            Finished finished = new(Sprint);
            Released newState = new(Sprint);
            //Act
            Sprint.SetSprintStatus(finished);
            Sprint.Status?.Release();

            //Assert          
            Assert.IsType(newState.GetType(), Sprint.Status);
        }
        [Fact]
        public void ReviewSprintStateFinishedDoesChangeWhenReviewed()
        {
            //Arrange
            ReviewSprint Sprint = new("testName", new DateTime(), new DateTime());
            Finished finished = new(Sprint);
            Reviewed newState = new(Sprint);
            //Act
            Sprint.SetSprintStatus(finished);
            Sprint.Status?.Review();

            //Assert          
            Assert.IsType(newState.GetType(), Sprint.Status);
        }
    }
}
