﻿using Domain;

namespace Tests
{
    public class BacklogTests
    {
        [Fact]
        public void BacklogItemslistIncreasedByOneWhenAddingItem()
        {
            //Arrange
            Backlog backlog = new();
            BacklogItem backlogItem = new(1, "testDescription");
            //Act
            backlog.AddItem(backlogItem);

            //Assert
            Assert.Equal(backlogItem, backlog.Items[0]);
            Assert.True(backlog.Items.Count.Equals(1));
        }
    }
}
