﻿using Domain;
using Domain.Thread;
using Domain.Visitors;

namespace Tests
{
    public class CommentTests
    {

        [Fact]
        public void CommentCreatedAndAddedToBacklogItem()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            Comment comment = new("testComment");
            ConcreteVisitor concreteThreadVisitor = new();

            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            backlogItem.Thread.AcceptVisitor(concreteThreadVisitor, 0);
            backlogItem.AddComment(comment, null);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Thread:", res);
        }
        [Fact]
        public void CommentCreatedAndNotAddedToBacklogItemBecauseStateIsDone()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            Comment comment = new("testComment");
            ConcreteVisitor concreteThreadVisitor = new();
            Domain.ItemStates.Done done = new(backlogItem);
            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            backlogItem.SetItemStatus(done);
            backlogItem.Thread.AcceptVisitor(concreteThreadVisitor, 0);
            backlogItem.AddComment(comment, null);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("backlog item is done, no changes allowed!", res);
        }
        [Fact]
        public void CommentCreatedAndAddedToMainCommentBacklogItem()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            Comment mainComment = new("mainComment");
            Comment comment = new("testComment");
            ConcreteVisitor concreteThreadVisitor = new();

            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act      
            backlogItem.AddComment(mainComment, null);
            backlogItem.AddComment(comment, mainComment);
            backlogItem.Thread.AcceptVisitor(concreteThreadVisitor, 0);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Thread:", res);
        }

        [Fact]
        public void CommentCreatedAndAlteredToBacklogItem()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            Comment comment = new("testComment");
            ConcreteVisitor concreteThreadVisitor = new();

            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            backlogItem.AddComment(comment, null);
            backlogItem.AlterComment(comment, "NewDescription");
            backlogItem.Thread.AcceptVisitor(concreteThreadVisitor, 0);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("NewDescription", res);
        }
        [Fact]
        public void CommentCreatedAndNotAlteredToBacklogItemBecauseStateIsDone()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            Comment comment = new("testComment");
            ConcreteVisitor concreteThreadVisitor = new();
            Domain.ItemStates.Done done = new(backlogItem);

            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            backlogItem.SetItemStatus(done);
            backlogItem.AddComment(comment, null);
            backlogItem.AlterComment(comment, "NewDescription");
            backlogItem.Thread.AcceptVisitor(concreteThreadVisitor, 0);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("backlog item is done, no changes allowed!", res);
        }


    }
}
