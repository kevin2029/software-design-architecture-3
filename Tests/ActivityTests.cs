﻿using Domain;
using Domain.ActivityStates;
using Domain.Actors;

namespace Tests
{
    public class ActivityTests
    {
        [Fact]
        public void ActivityCreatedWithDefaultStatusTodo()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            ToDo toDo = new ToDo(activity);

            //Act


            //Assert
            Assert.IsType(toDo.GetType(), activity.Status);
        }

        [Fact]
        public void ActivityStateChangedToInProgress()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            InProgress inProgress = new(activity);

            //Act
            activity.SetItemStatus(inProgress);

            //Assert
            Assert.IsType(inProgress.GetType(), activity.Status);
        }

        [Fact]
        public void ActivityStateChangedToDone()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            InProgress inProgress = new(activity);
            Done done = new(activity);

            //Act
            activity.SetItemStatus(inProgress);
            activity.SetItemStatus(done);

            //Assert
            Assert.IsType(done.GetType(), activity.Status);
        }

        [Fact]
        public void ActivityStateChangedToInProgressWhenPickedUp()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            InProgress inProgress = new(activity);

            //Act
            activity.Status.Pickup();

            //Assert
            Assert.IsType(inProgress.GetType(), activity.Status);
        }

        [Fact]
        public void ActivityStateChangedToTodoWhenGivenUp()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            InProgress inProgress = new(activity);
            ToDo state = new(activity);

            //Act
            activity.SetItemStatus(inProgress);
            activity.Status.GiveUp();

            //Assert
            Assert.IsType(state.GetType(), activity.Status);
        }
        [Fact]
        public void ActivityStateChangedToDoneWhenFinished()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            InProgress inProgress = new(activity);
            Done state = new(activity);

            //Act
            activity.SetItemStatus(inProgress);
            activity.Status.Finish();

            //Assert
            Assert.IsType(state.GetType(), activity.Status);
        }

        [Fact]
        public void ActivityStateTodoDoesNotChangeWhenFinished()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            ToDo todo = new(activity);
            Done done = new(activity);


            //Act
            activity.SetItemStatus(todo);
            activity.Status.Finish();

            //Assert
            Assert.IsType(todo.GetType(), activity.Status);
            Assert.IsNotType(done.GetType(), activity.Status);
        }
        [Fact]
        public void ActivityStateTodoDoesNotChangeWhenGivenUp()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            ToDo todo = new(activity);

            //Act
            activity.SetItemStatus(todo);
            activity.Status.GiveUp();

            //Assert
            Assert.IsType(todo.GetType(), activity.Status);
        }
        [Fact]
        public void ActivityStateInProgressDoesNotChangeWhenPickedUp()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            InProgress inProgress = new(activity);
            ToDo todo = new(activity);

            //Act
            activity.SetItemStatus(inProgress);
            activity.Status.Pickup();

            //Assert
            Assert.IsType(inProgress.GetType(), activity.Status);
            Assert.IsNotType(todo.GetType(), activity.Status);
        }

        [Fact]
        public void ActivityStateInProgressDoesChangeToTodoWhenGivenUp()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            InProgress inProgress = new(activity);
            ToDo todo = new(activity);

            //Act
            activity.SetItemStatus(inProgress);
            activity.Status.GiveUp();

            //Assert
            Assert.IsType(todo.GetType(), activity.Status);
            Assert.IsNotType(inProgress.GetType(), activity.Status);
        }
        [Fact]
        public void ActivityStateInProgressDoesChangeToDoneWhenFinished()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            InProgress inProgress = new(activity);
            Done done = new(activity);

            //Act
            activity.SetItemStatus(inProgress);
            activity.Status.Finish();

            //Assert
            Assert.IsType(done.GetType(), activity.Status);
            Assert.IsNotType(inProgress.GetType(), activity.Status);
        }

        [Fact]
        public void ActivityStateDoneDoesNotChangeWhenFinished()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            Done done = new(activity);
            ToDo todo = new(activity);
            InProgress inProgress = new(activity);

            //Act
            activity.SetItemStatus(done);
            activity.Status.Finish();

            //Assert
            Assert.IsType(done.GetType(), activity.Status);
            Assert.IsNotType(inProgress.GetType(), activity.Status);
            Assert.IsNotType(todo.GetType(), activity.Status);
        }
        [Fact]
        public void ActivityStateDoneDoesNotChangeWhenGivenUp()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            Done done = new(activity);
            ToDo todo = new(activity);
            InProgress inProgress = new(activity);

            //Act
            activity.SetItemStatus(done);
            activity.Status.GiveUp();

            //Assert
            Assert.IsType(done.GetType(), activity.Status);
            Assert.IsNotType(inProgress.GetType(), activity.Status);
            Assert.IsNotType(todo.GetType(), activity.Status);
        }
        [Fact]
        public void ActivityStateDoneDoesNotChangeWhenPickedUp()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            Done done = new(activity);
            ToDo todo = new(activity);
            InProgress inProgress = new(activity);

            //Act
            activity.SetItemStatus(done);
            activity.Status.Pickup();

            //Assert
            Assert.IsType(done.GetType(), activity.Status);
            Assert.IsNotType(inProgress.GetType(), activity.Status);
            Assert.IsNotType(todo.GetType(), activity.Status);
        }

        [Fact]
        public void SuccessActivityCanSetDeveloper()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");
            Developer developer = new("test", "Test@test.com", "066699985");

            //Act
            activity.SetAginee(developer);


            //Assert
            Assert.True(activity.Asignee?.Equals(developer));
        }
        [Fact]
        public void SuccessActivityNameCanBeChanged()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");


            //Act
            activity.Name = "test2";


            //Assert
            Assert.True(activity.Name.Equals("test2"));
        }
        [Fact]
        public void SuccessActivityDescriptionCanBeChanged()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");


            //Act
            activity.Description = "test2";


            //Assert
            Assert.True(activity.Description.Equals("test2"));
        }

        [Fact]
        public void SuccessActivityAsigneeCanBeNull()
        {
            //Arrange
            Activity activity = new("TestName", "TestDescription");

            //Act

            //Assert
            Assert.Null(activity.Asignee);
        }
    }
}
