﻿using Domain;

namespace Tests
{
    public class SprintTypeTests
    {
        [Fact]
        public void HasCorrectNumberOfValues()
        {
            var values = Enum.GetValues(typeof(SprintType));
            Assert.Equal(3, values.Length);
        }


        [Fact]
        public void CanParseFromString()
        {
            Assert.Equal(SprintType.Release, Enum.Parse(typeof(SprintType), "Release"));
            Assert.Equal(SprintType.Review, Enum.Parse(typeof(SprintType), "Review"));
            Assert.Equal(SprintType.Other, Enum.Parse(typeof(SprintType), "Other"));
        }
    }
}
