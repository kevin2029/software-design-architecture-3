﻿using Domain;
using Domain.ItemStates;
using Domain.Publisher;
using Domain.Thread;

namespace Tests
{

    public class PublisherTests
    {
        [Fact]
        public void BacklogItemStateGoingFromInprogessToReadyForTestingMailNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new MailSubscriber("testMail");

            Domain.ItemStates.InProgress inProgress = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(inProgress);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.Finish();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.Matches("Mail send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemStateGoingFromDoneToTodoMailNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new MailSubscriber("testMail");

            Done state = new(backlogItem);
            Todo todo = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.DoOver();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(todo.GetType(), backlogItem.Status);
            Assert.Matches("Mail send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemStateGoingFromReadyForTestingToTestingMailNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new MailSubscriber("testMail");

            ReadyForTesting state = new(backlogItem);
            Testing newState = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.Test();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(newState.GetType(), backlogItem.Status);
            Assert.Matches("Mail send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemStateGoingFromTestedToDoneMailNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new MailSubscriber("testMail");

            Tested state = new(backlogItem);
            Done newState = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.Complete();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(newState.GetType(), backlogItem.Status);
            Assert.Matches("Mail send for the backlogitem: 1", res);
        }
        [Fact]
        public void BacklogItemStateGoingFromTestingToTodoMailNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new MailSubscriber("testMail");

            Testing state = new(backlogItem);
            Todo newState = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.DoOver();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(newState.GetType(), backlogItem.Status);
            Assert.Matches("Mail send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemCommentAddedMailNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new MailSubscriber("testMail");
            Comment comment = new("testDescription");

            backlogItem.Subscribe(subscriber);


            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.AddComment(comment, null);
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();


            Assert.Matches("Mail send for the backlogitem: 1", res);
        }
        [Fact]
        public void BacklogItemCommentAlteredMailNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new MailSubscriber("testMail");
            Comment comment = new("testDescription");

            backlogItem.Subscribe(subscriber);


            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.AlterComment(comment, "NewDescription");
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.True(comment.Description.Equals("NewDescription"));
            Assert.Matches("Mail send for the backlogitem: 1", res);
        }

        [Fact]
        public void ReleaseSprintStatusChangedMailNotification()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            Domain.SprintStates.InProgress state = new(releaseSprint);
            ISubscriber subscriber = new MailSubscriber("testMail");
            releaseSprint.Subscribe(subscriber);
            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            releaseSprint.SetSprintStatus(state);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.Matches("Mail send for the sprint: testName", res);
        }


        [Fact]
        public void ReviewSprintStatusChangedMailNotification()
        {
            //Arrange
            ReleaseSprint sprint = new("testName", new DateTime(), new DateTime());
            Domain.SprintStates.InProgress state = new(sprint);
            ISubscriber subscriber = new MailSubscriber("testMail");
            sprint.Subscribe(subscriber);
            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            sprint.SetSprintStatus(state);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.Matches("Mail send for the sprint: testName", res);
        }
        [Fact]
        public void BacklogItemStateGoingFromInprogessToReadyForTestingSlackNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SlackSubscriber("testSlack");

            Domain.ItemStates.InProgress inProgress = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(inProgress);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.Finish();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.Matches("Slack send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemStateGoingFromDoneToTodoSlackNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SlackSubscriber("testSlack");

            Done state = new(backlogItem);
            Todo todo = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.DoOver();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(todo.GetType(), backlogItem.Status);
            Assert.Matches("Slack send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemStateGoingFromReadyForTestingToTestingSlackNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SlackSubscriber("testSlack");

            ReadyForTesting state = new(backlogItem);
            Testing newState = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.Test();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(newState.GetType(), backlogItem.Status);
            Assert.Matches("Slack send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemStateGoingFromTestedToDoneSlackNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SlackSubscriber("testSlack");

            Tested state = new(backlogItem);
            Done newState = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.Complete();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(newState.GetType(), backlogItem.Status);
            Assert.Matches("Slack send for the backlogitem: 1", res);
        }
        [Fact]
        public void BacklogItemStateGoingFromTestingToTodoSlackNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SlackSubscriber("testSlack");

            Testing state = new(backlogItem);
            Todo newState = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.DoOver();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(newState.GetType(), backlogItem.Status);
            Assert.Matches("Slack send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemCommentAddedSlackNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SlackSubscriber("testSlack");
            Comment comment = new("testDescription");

            backlogItem.Subscribe(subscriber);


            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.AddComment(comment, null);
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();


            Assert.Matches("Slack send for the backlogitem: 1", res);
        }
        [Fact]
        public void BacklogItemCommentAlteredSlackNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SlackSubscriber("testSlack");
            Comment comment = new("testDescription");

            backlogItem.Subscribe(subscriber);


            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.AlterComment(comment, "NewDescription");
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.True(comment.Description.Equals("NewDescription"));
            Assert.Matches("Slack send for the backlogitem: 1", res);
        }

        [Fact]
        public void ReleaseSprintStatusChangedSlackNotification()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            Domain.SprintStates.InProgress state = new(releaseSprint);
            ISubscriber subscriber = new SlackSubscriber("testSlack");
            releaseSprint.Subscribe(subscriber);
            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            releaseSprint.SetSprintStatus(state);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.Matches("Slack send for the sprint: testName", res);
        }


        [Fact]
        public void ReviewSprintStatusChangedSlackNotification()
        {
            //Arrange
            ReleaseSprint sprint = new("testName", new DateTime(), new DateTime());
            Domain.SprintStates.InProgress state = new(sprint);
            ISubscriber subscriber = new SlackSubscriber("testSlack");
            sprint.Subscribe(subscriber);
            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            sprint.SetSprintStatus(state);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.Matches("Slack send for the sprint: testName", res);
        }
        [Fact]
        public void BacklogItemStateGoingFromInprogessToReadyForTestingSmsNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SmsSubscriber("testSms");

            Domain.ItemStates.InProgress inProgress = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(inProgress);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.Finish();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.Matches("Sms send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemStateGoingFromDoneToTodoSmsNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SmsSubscriber("testSms");

            Done state = new(backlogItem);
            Todo todo = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.DoOver();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(todo.GetType(), backlogItem.Status);
            Assert.Matches("Sms send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemStateGoingFromReadyForTestingToTestingSmsNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SmsSubscriber("testSms");

            ReadyForTesting state = new(backlogItem);
            Testing newState = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.Test();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(newState.GetType(), backlogItem.Status);
            Assert.Matches("Sms send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemStateGoingFromTestedToDoneSmsNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SmsSubscriber("testSms");

            Tested state = new(backlogItem);
            Done newState = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.Complete();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(newState.GetType(), backlogItem.Status);
            Assert.Matches("Sms send for the backlogitem: 1", res);
        }
        [Fact]
        public void BacklogItemStateGoingFromTestingToTodoSmsNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SmsSubscriber("testSms");

            Testing state = new(backlogItem);
            Todo newState = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.Status.DoOver();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(newState.GetType(), backlogItem.Status);
            Assert.Matches("Sms send for the backlogitem: 1", res);
        }

        [Fact]
        public void BacklogItemCommentAddedSmsNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SmsSubscriber("testSms");
            Comment comment = new("testDescription");

            backlogItem.Subscribe(subscriber);


            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.AddComment(comment, null);
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();


            Assert.Matches("Sms send for the backlogitem: 1", res);
        }
        [Fact]
        public void BacklogItemCommentAlteredSmsNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SmsSubscriber("testSms");
            Comment comment = new("testDescription");

            backlogItem.Subscribe(subscriber);


            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.AlterComment(comment, "NewDescription");
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.True(comment.Description.Equals("NewDescription"));
            Assert.Matches("Sms send for the backlogitem: 1", res);
        }

        [Fact]
        public void ReleaseSprintStatusChangedSmsNotification()
        {
            //Arrange
            ReleaseSprint releaseSprint = new("testName", new DateTime(), new DateTime());
            Domain.SprintStates.InProgress state = new(releaseSprint);
            ISubscriber subscriber = new SmsSubscriber("testSms");
            releaseSprint.Subscribe(subscriber);
            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            releaseSprint.SetSprintStatus(state);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.Matches("Sms send for the sprint: testName", res);
        }


        [Fact]
        public void ReviewSprintStatusChangedSmsNotification()
        {
            //Arrange
            ReleaseSprint sprint = new("testName", new DateTime(), new DateTime());
            Domain.SprintStates.InProgress state = new(sprint);
            ISubscriber subscriber = new SmsSubscriber("testSms");
            sprint.Subscribe(subscriber);
            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            sprint.SetSprintStatus(state);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.Matches("Sms send for the sprint: testName", res);
        }

        [Fact]
        public void ReviewSprintSubscriberUnSubbedNotReceivingNotification()
        {
            //Arrange
            ReviewSprint sprint = new("testName", new DateTime(), new DateTime());
            Domain.SprintStates.InProgress state = new(sprint);
            ISubscriber subscriber = new SmsSubscriber("testSms");
            sprint.Subscribe(subscriber);
            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            sprint.UnSubscribe(subscriber);
            sprint.SetSprintStatus(state);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.NotEqual("Sms send for the sprint: testName", res);
        }

        [Fact]
        public void ReleaseSprintSubscriberUnSubbedNotReceivingNotification()
        {
            //Arrange
            ReleaseSprint sprint = new("testName", new DateTime(), new DateTime());
            Domain.SprintStates.InProgress state = new(sprint);
            ISubscriber subscriber = new SmsSubscriber("testSms");
            sprint.Subscribe(subscriber);
            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            sprint.UnSubscribe(subscriber);
            sprint.SetSprintStatus(state);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.NotEqual("Sms send for the sprint: testName", res);
        }

        [Fact]
        public void BacklogItemSubscriberUnSubbedNotReceivingNotification()
        {
            //Arrange
            BacklogItem backlogItem = new(1, "testDescription");
            ISubscriber subscriber = new SmsSubscriber("testSms");

            Testing state = new(backlogItem);
            Todo newState = new(backlogItem);
            backlogItem.Subscribe(subscriber);
            backlogItem.SetItemStatus(state);

            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            backlogItem.UnSubscribe(subscriber);
            backlogItem.Status.DoOver();
            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();

            Assert.IsType(newState.GetType(), backlogItem.Status);
            Assert.NotEqual("Sms send for the backlogitem: 1", res);
        }

        [Fact]
        public void MailSubscriberUpdateRetursCorrectLine()
        {
            //Arrange
            MailSubscriber subscriber = new("test");
            BacklogItem backlogItem = new(1, "testDescription");
            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            subscriber.Update(backlogItem);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Mail send for the backlogitem: 1", res);
        }

        [Fact]
        public void MailSubscriberUpdateSprintRetursCorrectLine()
        {
            //Arrange
            MailSubscriber subscriber = new("test");
            ReleaseSprint sprint = new("testName", new DateTime(), new DateTime());
            var writer = new StringWriter();
            Console.SetOut(writer);

            //Act
            subscriber.UpdateSprint(sprint);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Mail send for the sprint: testName", res);
        }
        [Fact]
        public void SmsSubscriberUpdateRetursCorrectLine()
        {
            //Arrange
            SmsSubscriber subscriber = new("test");
            BacklogItem backlogItem = new(1, "testDescription");
            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            subscriber.Update(backlogItem);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Sms send for the backlogitem: 1", res);
        }

        [Fact]
        public void SmsSubscriberUpdateSprintRetursCorrectLine()
        {
            //Arrange
            SmsSubscriber subscriber = new("test");
            ReleaseSprint sprint = new("testName", new DateTime(), new DateTime());
            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            subscriber.UpdateSprint(sprint);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Sms send for the sprint: testName", res);
        }
        [Fact]
        public void SlackSubscriberUpdateRetursCorrectLine()
        {
            //Arrange
            SlackSubscriber subscriber = new("test");
            BacklogItem backlogItem = new(1, "testDescription");
            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            subscriber.Update(backlogItem);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Slack send for the backlogitem: 1", res);
        }

        [Fact]
        public void SlackSubscriberUpdateSprintRetursCorrectLine()
        {
            //Arrange
            SlackSubscriber subscriber = new("test");
            ReleaseSprint sprint = new("testName", new DateTime(), new DateTime());
            var writer = new StringWriter();
            Console.SetOut(writer);
            //Act
            subscriber.UpdateSprint(sprint);

            //Assert
            var sb = writer.GetStringBuilder();
            var res = sb.ToString().Trim();
            Assert.Matches("Slack send for the sprint: testName", res);
        }
    }
}
